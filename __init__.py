# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import shipment


def register():
    Pool.register(
        shipment.PrintValuedDeliveryNoteStart,
        module='stock_valued_report', type_='model')
    Pool.register(
        shipment.PrintValuedDeliveryNote,
        shipment.PrintValuedRestockingList,
        module='stock_valued_report', type_='wizard')
    Pool.register(
        shipment.DeliveryNote,
        module='stock_valued_report', type_='report')
    Pool.register(
        shipment.ShipmentOutDoAndPrint,
        module='stock_valued_report', type_='wizard',
        depends=['stock_shipment_out_go_and_print'])
